# -*- coding: UTF8 -*-

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings 
from django.shortcuts import redirect

from experiment.models import *
from django.contrib.auth.models import User

import socket

LAB_MANAGER_IP = '127.0.0.1'
LAB_MANAGER_PORT = 55555

@login_required(login_url='/auth/')

def exp_prepare(request, experiment_ether):
	exp_obj = exist_experiments.objects.get(id = experiment_ether)
	usr_obj = User.objects.get(username=request.user)

	context = { 'exp_obj': exp_obj, 'usr_obj': usr_obj, 'experiment_ether': experiment_ether}
	return render(request, 'exp_ADC1/exp_prepare.html', context)

def exp_start(request, experiment_ether):
	obj_exp = exist_experiments.objects.get(id=experiment_ether)
	obj_usr = User.objects.get(username=request.user)	
	
	type_choice = "type1"

	socket_to_manager = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	socket_to_manager.connect((LAB_MANAGER_IP, LAB_MANAGER_PORT))	
	socket_to_manager.send("make-experiment" + " " + str(experiment_ether) + " " + str(obj_exp.experiment_app) + " " + type_choice)
	
	manager_reply = socket_to_manager.recv(1024)
	socket_to_manager.close()

	manager_reply_status = manager_reply.split()[0]
	manager_reply_file = manager_reply.split()[1]

	if manager_reply_status == 'busy':
		tempResult = results.objects.get(experiment=obj_exp, done = False)
		
		if tempResult.userid == obj_usr:
			context = { 'newExperiment': tempResult, 'manager_reply_status': 'myExperiment', 'experiment_id': tempResult.id,}
			return render(request, 'exp_running.html', context)	
		else:
			context = { 'manager_reply_status': 'busy', }
			return render(request, 'exp_running.html', context)	

	else:
		new_result = results(experiment=obj_exp, userid=obj_usr, buff_file=manager_reply_file)
		new_result.save()	

		context = { 'newExperiment': new_result, 'manager_reply_status': manager_reply_status, 'experiment_id': new_result.id,  }
		return render(request, 'exp_ADC1/exp_running.html', context)

def exp_stop(request, experimentID):
	obj_usr = User.objects.get(username=request.user)
	tempResult = results.objects.get(id=experimentID, userid=obj_usr, done=False)
	tempResult.done = True
	tempResult.save()

	socket_to_manager = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	socket_to_manager.connect((LAB_MANAGER_IP, LAB_MANAGER_PORT))
	socket_to_manager.send("kill-experiment" + " " + str(tempResult.experiment.id))

	manager_reply = socket_to_manager.recv(1024)
	socket_to_manager.close()

	context = { 'manager_reply': manager_reply,}
	return render(request, 'exp_ADC1/exp_stop.html', context)

def exp_result(request, experimentID):
	usr_obj = User.objects.get(username=request.user)
	res_obj = results.objects.get(id=experimentID, userid=usr_obj, done=True)

	context = { 'res_obj': res_obj, 'usr_obj': usr_obj, 'experimentID': experimentID}
	return render(request, 'exp_ADC1/exp_result.html', context)

def exp_del(request, experimentID):
	if request.method == 'POST':
		tempResult = results.objects.get(id=experimentID)
		
		socket_to_manager = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		socket_to_manager.connect((LAB_MANAGER_IP, LAB_MANAGER_PORT))	
		socket_to_manager.send("del-results" + " " + str(tempResult.buff_file))
		manager_reply = socket_to_manager.recv(1024)
		socket_to_manager.close()

		tempResult.delete()

	return redirect('/main/')