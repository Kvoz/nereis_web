from django.conf.urls import patterns, url

from exp_ADC1 import views

urlpatterns = patterns('',
	url(r'^preview/(?P<experiment_ether>\d+)/$', views.exp_prepare, name='prep_ADC1'),
	url(r'^start/(?P<experiment_ether>\d+)/$', views.exp_start, name='start_ADC1'),
	url(r'^stop/(?P<experimentID>\d+)/$', views.exp_stop, name='stop_ADC1'),
	url(r'^show/(?P<experimentID>\d+)/$', views.exp_result, name='show_ADC1'),
	url(r'^delete/(?P<experimentID>\d+)/$', views.exp_del, name='delete_ADC1'),

	# url(r'^(?P<experiment_ether>\d+)/$', views.make_experiment, name='makeExperiment'),
	# url(r'^show(?P<experimentID>\d+)/delete/$', views.del_results, name='deleteExperiment'),
	# url(r'^show(?P<experimentID>\d+)/$', views.show_results, name='showExperiment'),
	# url(r'^kill(?P<experiment_ether>\d+)/$', views.kill_experiment, name='killExperiment'),
	
)