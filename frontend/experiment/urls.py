from django.conf.urls import patterns, url

from experiment import views

urlpatterns = patterns('',
	url(r'^(?P<experiment_ether>\d+)/$', views.make_experiment, name='makeExperiment'),
	url(r'^show(?P<experimentID>\d+)/delete/$', views.del_results, name='deleteExperiment'),
	url(r'^show(?P<experimentID>\d+)/$', views.show_results, name='showExperiment'),
	url(r'^kill(?P<experiment_ether>\d+)/$', views.kill_experiment, name='killExperiment'),
	
)