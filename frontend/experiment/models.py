from django.db import models
from django.contrib.auth.models import User


class exist_experiments(models.Model):
	# Auto ID field
	experiment_name = models.CharField(max_length=200)
	experiment_address = models.GenericIPAddressField(protocol='IPv4')
	experiment_app = models.CharField(max_length=200)


class results(models.Model):
	experiment = models.ForeignKey(exist_experiments)
	userid = models.ForeignKey(User)
	exp_type = models.CharField(max_length=50)
	buff_file = models.CharField(max_length=200)
	date = models.DateField(auto_now=True)
	done = models.BooleanField(default=False)
	

