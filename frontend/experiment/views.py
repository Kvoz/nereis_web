# -*- coding: UTF8 -*-

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings 

from django.contrib.auth import logout
from django.shortcuts import redirect

from experiment.models import *
from django.contrib.auth.models import User

import socket
LAB_MANAGER_IP = '127.0.0.1'
LAB_MANAGER_PORT = 55555


@login_required(login_url='/auth/')

def show_results(request, experimentID):
	# obj_usr = User.objects.get(username=request.user)
	tempResult = results.objects.get(id=experimentID)
	context = { 'fromKilling': False, 'resultExperiments': tempResult, }
	return render(request, 'results.html', context)


def del_results(request, experimentID):
	if request.method == 'POST':
		tempResult = results.objects.get(id=experimentID)
		
		socket_to_manager = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		socket_to_manager.connect((LAB_MANAGER_IP, LAB_MANAGER_PORT))	
		socket_to_manager.send("del-results" + " " + str(tempResult.buff_file))
		manager_reply = socket_to_manager.recv(1024)
		socket_to_manager.close()

		tempResult.delete()

	return redirect('/main/')


def make_experiment(request, experiment_ether):
	socket_to_manager = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	socket_to_manager.connect((LAB_MANAGER_IP, LAB_MANAGER_PORT))	
	socket_to_manager.send("make-experiment" + " " + str(experiment_ether))
	
	manager_reply = socket_to_manager.recv(1024)
	socket_to_manager.close()

	manager_reply_status = manager_reply.split()[0]
	manager_reply_file = manager_reply.split()[1]

	if manager_reply_status == 'busy':
		obj_exp = exist_experiments.objects.get(id=experiment_ether)
		obj_usr = User.objects.get(username=request.user)	
		tempResult = results.objects.get(experiment=obj_exp, done = False)
		
		if tempResult.userid == obj_usr:
			context = { 'newExperiment': tempResult, 'manager_reply_status': 'myExperiment', }
			return render(request, 'make_experiment.html', context)	
		else:
			context = { 'newExperiment': '', 'manager_reply_status': 'busy', }
			return render(request, 'make_experiment.html', context)	

	else:
		obj_exp = exist_experiments.objects.get(id=experiment_ether)
		obj_usr = User.objects.get(username=request.user)	
		new_result = results(experiment=obj_exp, userid=obj_usr, buff_file=manager_reply_file)
		new_result.save()	

		context = { 'newExperiment': new_result, 'manager_reply_status': manager_reply_status, }
		return render(request, 'make_experiment.html', context)


def kill_experiment(request, experiment_ether):

	socket_to_manager = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	socket_to_manager.connect((LAB_MANAGER_IP, LAB_MANAGER_PORT))
	socket_to_manager.send("kill-experiment" + " " + str(experiment_ether))

	manager_reply = socket_to_manager.recv(1024)
	socket_to_manager.close()

	obj_exp = exist_experiments.objects.get(id=experiment_ether)
	obj_usr = User.objects.get(username=request.user)
	tempResult = results.objects.get(experiment=obj_exp, userid=obj_usr, done=False)
	tempResult.done = True
	tempResult.save()

	context = { 'fromKilling': True,  'manager_reply': manager_reply,}
	return render(request, 'results.html', context)
	