# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='exist_experiments',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('experiment_name', models.CharField(max_length=200)),
                ('experiment_address', models.GenericIPAddressField(protocol=b'IPv4')),
            ],
        ),
        migrations.CreateModel(
            name='results',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('buff_file', models.CharField(max_length=200)),
                ('date', models.DateField(auto_now=True)),
                ('done', models.BooleanField(default=False)),
                ('experiment', models.ForeignKey(to='experiment.exist_experiments')),
                ('userid', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
