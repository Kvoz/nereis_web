# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('experiment', '0002_exist_experiments_experiment_app'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='exist_experiments',
            name='experiment_app',
        ),
    ]
