# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('experiment', '0004_exist_experiments_experiment_app'),
    ]

    operations = [
        migrations.AddField(
            model_name='results',
            name='exp_type',
            field=models.CharField(default='type_1', max_length=50),
            preserve_default=False,
        ),
    ]
