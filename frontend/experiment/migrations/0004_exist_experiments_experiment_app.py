# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('experiment', '0003_remove_exist_experiments_experiment_app'),
    ]

    operations = [
        migrations.AddField(
            model_name='exist_experiments',
            name='experiment_app',
            field=models.CharField(default='x', max_length=200),
            preserve_default=False,
        ),
    ]
