from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	# Examples:
	# url(r'^$', 'frontend.views.home', name='home'),
	# url(r'^blog/', include('blog.urls')),

	url(r'^auth/', include('nereisauth.urls')),
	url(r'^main/', include('mainroom.urls')),
	url(r'^experiment/', include('experiment.urls')),
	url(r'^admin/', include(admin.site.urls)),

	# Experiments
	url(r'^exp_ADC1/', include('exp_ADC1.urls')),	
	url(r'^exp_hypersonic/', include('exp_hypersonic.urls')),	
)
