# -*- coding: UTF8 -*-

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings 

from django.contrib.auth import logout
from django.shortcuts import redirect

from experiment.models import *
from django.contrib.auth.models import User


@login_required(login_url='/auth/')

def overview(request):
	valid_experiments = exist_experiments.objects.order_by()
	obj_usr = User.objects.get(username=request.user)
	tempResult = results.objects.filter(userid=obj_usr, done=True)
	runningExperiments = results.objects.filter(userid=obj_usr, done=False)
	# print runningExperiments
	context = { 'existExperiments': valid_experiments, 'resultExperiments': tempResult, 'runningExperiments': runningExperiments, }
	return render(request, 'mainroom.html', context)

def logout_view(request):
	obj_usr = User.objects.get(username=request.user)	
	busyUser = results.objects.filter(userid=obj_usr, done = False)

	if busyUser:
		valid_experiments = exist_experiments.objects.order_by()
		tempResult = results.objects.filter(userid=obj_usr, done = True)
		runningExperiments = results.objects.filter(userid=obj_usr, done=False)
		context = { 'existExperiments': valid_experiments, 'resultExperiments': tempResult, 'runningExperiments': runningExperiments, 'failLogout': True}
		return render(request, 'mainroom.html', context)
	else:
		logout(request)
		return redirect('/auth/')