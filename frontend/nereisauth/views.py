# -*- coding: UTF8 -*-

from django.shortcuts import render

from django.contrib import auth
from django.shortcuts import redirect

from django import forms
from django.utils.translation import ugettext as _

#forms for auth page
class UserLoginForm(forms.Form):
    username = forms.CharField(label=_(u'Пользователь'), max_length=30)
    password = forms.CharField(label=_(u'Пароль'), widget=forms.PasswordInput)




def index(request):
    form = UserLoginForm(request.POST or None)
    context = { 'form': form, }
    if request.method == 'POST' and form.is_valid():
        username = form.cleaned_data.get('username', None)
        password = form.cleaned_data.get('password', None)
        user = auth.authenticate(username=username, password=password)
        if user and user.is_active:
            auth.login(request, user)
            return redirect('/main/')
    return render(request, 'index.html', context)