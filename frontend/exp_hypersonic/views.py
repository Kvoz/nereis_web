# -*- coding: UTF8 -*-
import sys, os

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.conf import settings 
from django.shortcuts import redirect

# from django import forms
from exp_hypersonic.forms import exp_type
from exp_hypersonic.forms import exp_params

from django.templatetags.static import static

from experiment.models import *
from django.contrib.auth.models import User

import socket

LAB_MANAGER_IP = '127.0.0.1'
LAB_MANAGER_PORT = 55555

# Переменные, содержащие пути к папке менеджера лаборатории, папкам для временного хранения результатов и рисунков
LAB_DIR = os.path.dirname(os.path.dirname(__file__))
WHERE_TO_STORE_DAT = os.path.join(os.path.join(LAB_DIR, os.pardir), 'exp_dat')



@login_required(login_url='/auth/')


def exp_prepare(request, experiment_ether):
	exp_obj = exist_experiments.objects.get(id = experiment_ether)
	usr_obj = User.objects.get(username=request.user)

	exp_type_choice = exp_type()
	context = { 'exp_obj': exp_obj, 'usr_obj': usr_obj, 'experiment_ether': experiment_ether, \
				'exp_type_choice': exp_type_choice,}
	return render(request, 'exp_hypersonic/exp_prepare.html', context)

def exp_start(request, experiment_ether):
	# Получение объектов из БД по эксперименту и по пользователю
	obj_exp = exist_experiments.objects.get(id=experiment_ether)
	obj_usr = User.objects.get(username=request.user)

	# Получение значение полей предустановок
	if request.method == 'POST': 
		exp_preset = exp_type(request.POST) # Получение форм из реквеста
		if exp_preset.is_valid(): # Проверка (необходима, чтобы появился атрибут cleaned_data)
			# Выбор из словаря значений полей формы
			type_choice = str(exp_preset.cleaned_data['type_choice'] )
	else:
		tempResult = results.objects.get(experiment=obj_exp, done = False)
		type_choice = tempResult.exp_type

	# Связь с менеджером лаборатории с запросом о старте эксперимента
	socket_to_manager = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	socket_to_manager.connect((LAB_MANAGER_IP, LAB_MANAGER_PORT))	
	socket_to_manager.send("make-experiment" + " " + str(experiment_ether) + " " + str(obj_exp.experiment_app) + " " + type_choice)
	
	manager_reply = socket_to_manager.recv(1024)
	socket_to_manager.close()

	manager_reply_status = manager_reply.split()[0]
	
	# Проверка ответа от менеджера лаборатории
	# статус busy => эксперимент уже запущен пользователем или кем-то другим
	if manager_reply_status == 'busy':
		tempResult = results.objects.get(experiment=obj_exp, done = False)
		type_choice = tempResult.exp_type
		
		if tempResult.userid == obj_usr:

			if type_choice == "type_1":
				context = { 'newExperiment': tempResult, 'manager_reply_status': 'myExperiment', \
							'experiment_id': tempResult.id,}
				return render(request, 'exp_hypersonic/exp_running1.html', context)	
			elif type_choice == "type_2":
				exp_setopt_form = exp_params()

				returnedValsFile = os.path.join(os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), os.pardir)), 'exp_dat') + "/" + os.path.basename(tempResult.buff_file)

				fo = open(returnedValsFile + ".dat")
				returnedVals = fo.readline()
				fo.close()

				fo = open(returnedValsFile + ".dat.cfg")
				cfgStatusNow = fo.readlines()
				fo.close()

				# laserStatus = returnedVals.split()[1]
				distanceVal = returnedVals.split()[0]

				angle_status = cfgStatusNow[1]
				laserStatus = cfgStatusNow[0]

				context = { 'newExperiment': tempResult, 'manager_reply_status': 'myExperiment', \
							'experiment_id': tempResult.id, 'exp_setopt_form' : exp_setopt_form, \
							'experiment_ether': experiment_ether, 'laserStatus': laserStatus, \
							'distanceVal': distanceVal, 'angle_status': angle_status, }
				return render(request, 'exp_hypersonic/exp_running2.html', context)	
		else:
			context = { 'manager_reply_status': 'busy', }
			return render(request, 'exp_hypersonic/exp_running1.html', context)
	# blocked => эксперимент блокирован локальной консолью
	elif manager_reply_status == "blocked":
		context = { 'manager_reply_status': 'blocked', }
		return render(request, 'exp_hypersonic/exp_running1.html', context)

	# Эксперимент не запущен и инициируется процесс подготовки эксперимента
	else:

		manager_reply_file = manager_reply.split()[1]

		new_result = results(experiment=obj_exp, userid=obj_usr, buff_file=manager_reply_file, exp_type=type_choice )
		new_result.save()	

		# Default values
		laser_status = "0"
		servo_angle = "0"

		cfgDirName = os.path.join(os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), os.pardir)), 'exp_dat') + "/" + os.path.basename(manager_reply_file)
		
		fo = open(cfgDirName + ".dat.cfg", "w")
		fo.write(laser_status + "\n" + servo_angle)
		fo.close()

		fo = open(cfgDirName + ".dat", "w")
		fo.write('')
		fo.close()

		if type_choice == "type_1":
			context = { 'newExperiment': new_result, 'manager_reply_status': manager_reply_status, \
						'experiment_id': new_result.id, }
			return render(request, 'exp_hypersonic/exp_running1.html', context)	
		elif type_choice == "type_2":
			exp_setopt_form = exp_params()
			context = { 'newExperiment': new_result, 'manager_reply_status': manager_reply_status, \
						'experiment_id': new_result.id, "exp_setopt_form" : exp_setopt_form, \
						'experiment_ether': experiment_ether, 'laserStatus': laser_status, \
						'distanceVal': 'n/a', 'angle_status': servo_angle, }
			return render(request, 'exp_hypersonic/exp_running2.html', context)	


def exp_stop(request, experimentID):
	obj_usr = User.objects.get(username=request.user)
	tempResult = results.objects.get(id=experimentID, userid=obj_usr, done=False)
	tempResult.done = True
	tempResult.save()

	socket_to_manager = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	socket_to_manager.connect((LAB_MANAGER_IP, LAB_MANAGER_PORT))
	socket_to_manager.send("kill-experiment" + " " + str(tempResult.experiment.id))

	manager_reply = socket_to_manager.recv(1024)
	socket_to_manager.close()

	context = { 'manager_reply': manager_reply,}
	return render(request, 'exp_hypersonic/exp_stop.html', context)

def exp_result(request, experimentID):
	usr_obj = User.objects.get(username=request.user)
	res_obj = results.objects.get(id=experimentID, userid=usr_obj, done=True)

	context = { 'res_obj': res_obj, 'usr_obj': usr_obj, 'experimentID': experimentID}
	return render(request, 'exp_hypersonic/exp_result.html', context)

def exp_del(request, experimentID):
	if request.method == 'POST':
		tempResult = results.objects.get(id=experimentID)
		
		socket_to_manager = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		socket_to_manager.connect((LAB_MANAGER_IP, LAB_MANAGER_PORT))	
		socket_to_manager.send("del-results" + " " + str(tempResult.buff_file))
		manager_reply = socket_to_manager.recv(1024)
		socket_to_manager.close()

		tempResult.delete()

	return redirect('/main/')

def exp_setoption(request, experiment_ether):
	obj_exp = exist_experiments.objects.get(id=experiment_ether)
	tempResult = results.objects.get(experiment=obj_exp, done = False)

	if request.method == 'POST': 
		experiment_options = exp_params(request.POST) # A form bound to the POST data
		if experiment_options.is_valid(): # All validation rules pass
			laser_status = str(experiment_options.cleaned_data['laser_status'] )
			servo_angle = str(experiment_options.cleaned_data['angle'] )
			# print dataTemp
		else:
			# отработка ошибки пустого поля поворота угла
			exp_setopt_form = exp_params()

			returnedValsFile = os.path.join(os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), os.pardir)), 'exp_dat') + "/" + os.path.basename(tempResult.buff_file)

			fo = open(returnedValsFile + ".dat")
			returnedVals = []
			for line in fo:
				returnedVals.append(line)
			fo.close()

			laserStatus = returnedVals[0]
			distanceVal = "Ошибка. Пустой аргумент!"

			context = { 'newExperiment': tempResult, 'manager_reply_status': 'myExperiment', \
						'experiment_id': tempResult.id, 'exp_setopt_form' : exp_setopt_form, \
						'experiment_ether': experiment_ether, 'laserStatus': laserStatus, \
						'distanceVal': distanceVal,}
			return render(request, 'exp_hypersonic/exp_running2.html', context)	

	obj_exp = exist_experiments.objects.get(id=experiment_ether)
	experiment_info = results.objects.get(experiment=obj_exp, done = False)

	DatCfgUrl = os.path.join(WHERE_TO_STORE_DAT, experiment_info.buff_file) + ".dat.cfg"
	
	fo = open(DatCfgUrl, "r")
	cfgList = fo.readlines()
	angleCfg = int(cfgList[1])
	fo.close()

	if request.POST.get('inc_angle'):
		angleCfg = angleCfg + int(servo_angle)
		if angleCfg > 180:
			angleCfg = 180
	elif request.POST.get('dec_angle'):
		angleCfg = angleCfg - int(servo_angle)
		if angleCfg < 0:
			angleCfg = 0

	fo = open(DatCfgUrl, "w")
	fo.write(laser_status + "\n" + str(angleCfg))
	fo.close()

	return redirect('/' + str(experiment_info.experiment.experiment_app) + "/start/" + str(experiment_info.experiment.id)+"/")



	