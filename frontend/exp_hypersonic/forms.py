# -*- coding: UTF8 -*-

from django import forms

# from experiment.models import *
# from django.contrib.auth.models import User


#forms for exp_hypersonic page
class exp_type(forms.Form):
	CHOICES = [('type_1','Тип 1'),('type_2','Тип 2')]
	type_choice = forms.ChoiceField(label= 'Тип эксперимента' ,choices=CHOICES)


class exp_params(forms.Form):
	CHOICES = [('1','Вкл.'),('0','Выкл.')]
	laser_status = forms.ChoiceField(label= 'Указатель' ,choices=CHOICES)

	angle = forms.CharField(max_length=3, min_length=1, initial='0')
