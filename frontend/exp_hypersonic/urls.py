from django.conf.urls import patterns, url

from exp_hypersonic import views

urlpatterns = patterns('',
	url(r'^preview/(?P<experiment_ether>\d+)/$', views.exp_prepare, name='prep_hypersonic'),
	url(r'^start/(?P<experiment_ether>\d+)/$', views.exp_start, name='start_hypersonic'),
	url(r'^stop/(?P<experimentID>\d+)/$', views.exp_stop, name='stop_hypersonic'),
	url(r'^show/(?P<experimentID>\d+)/$', views.exp_result, name='show_hypersonic'),
	url(r'^delete/(?P<experimentID>\d+)/$', views.exp_del, name='delete_hypersonic'),

	url(r'^setopt/(?P<experiment_ether>\d+)/$', views.exp_setoption, name='setopt_hypersonic'),
)