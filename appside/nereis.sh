#! /bin/bash

PIDFILE="pid"

runNereis(){
	echo "Starting WEB server"
	xterm -title "Frontend NEREIS" -e "python ../frontend/manage.py runserver" &

	sleep 1

	echo $! > $PIDFILE

	echo "Starting LABMANAGER"
	xterm -title "Labmanager NEREIS" -e "python labmanager.py" &	
	
	sleep 1

	echo $! >> $PIDFILE
}

stopNereis(){
	while read line; do
		kill -SIGTERM $line
	done < $PIDFILE

	echo "" > $PIDFILE
}


restartNereis(){
	stopNereis
	sleep 1
	runNereis
}



if [ $1 = "run" ]; then
	runNereis
elif [ $1 = "stop" ]; then
	stopNereis
elif [ $1 = "restart" ]; then
	restartNereis	
else
	echo "Ошибка в аргументах"
fi

exit 0




