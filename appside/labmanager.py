# -*- coding: UTF8 -*-

import socket
import subprocess
import random, string
import os

# import threading

# Параметры сокета менеджера лаборатории
manager_IP = "127.0.0.1"
manager_port = 55555

# Переменные, содержащие пути к папке менеджера лаборатории, папкам для временного хранения результатов и рисунков
LAB_DIR = os.path.dirname(os.path.dirname(__file__))
WHERE_TO_STORE_IMG = os.path.join(os.path.join(LAB_DIR, os.pardir), 'exp_img')
WHERE_TO_STORE_DAT = os.path.join(os.path.join(LAB_DIR, os.pardir), 'exp_dat')

# Словарь, содержащий experiment_ether активных экспериментов
present_experiments = {}

# Процедура для генерации случайного имени файла
def randomword(length):
   return ''.join(random.choice(string.lowercase) for i in range(length))


# Создание серверного сокета
manager_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
manager_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
manager_socket.bind((manager_IP, manager_port)) 
manager_socket.listen(1)

print "Labmanager started on " + manager_IP + ":" + str(manager_port)

# Подключение клиентов в бесконечном цикле
while 1:
	try:
		(client_socket, client_addr) = manager_socket.accept()	
	except:
		break
	
	# Получение команды клиента
	data = client_socket.recv(1024)
	data = data.split()

	# Первой ячейкой -- команда
	command = data[0]


	if command == "make-experiment":
		# Второй ячейкой -- experiment_ether (ID по таблице exist_experiments) или имя файла-эксперимента
		# Третьей ячейкой -- имя experiment_app для выбора корректного модуля подключения
		experiment_ether = data[1]
		experiment_app = data[2]
		experiment_options = data[3:]

		child_check_exp_status = subprocess.Popen(["python", str(experiment_app) + "/check.py"], stdout=subprocess.PIPE)
		exp_status, check_errs = child_check_exp_status.communicate()

		# Если эксперимент есть в списке активных или занят оператором, то вернуть состояние busy
		if experiment_ether in present_experiments.keys():
			client_socket.sendto("busy busy", client_addr)
			client_socket.close()
			continue
		# Проверка доступности удалённого устройства
		elif exp_status.split()[0] == "blocked":
			client_socket.sendto("blocked blocked", client_addr)
			client_socket.close()
			continue
		else:
			# Сгенерировать временный файл для аккумуляции данных
			dataTempFile = randomword(15)
			buff_file = os.path.join(WHERE_TO_STORE_DAT, dataTempFile) + ".dat"
			# Запустить субпроцесс для подключения к дистанционному модулю
			child_pid = subprocess.Popen(["python", str(experiment_app) + "/" + experiment_options[0] + ".py"] + [buff_file])
			# Обновить состояние словаря текущих экспериментов
			present_experiments[experiment_ether] = child_pid

			# Вернуть метку успешного запуска эксперимента в django
			client_socket.sendto("started " + dataTempFile, client_addr)
			client_socket.close()
			print "Experiment ether #" + experiment_ether + " started!" + "Child on #" + str(present_experiments[experiment_ether].pid)
			
			# DEBUG LEVEL
			# В этой переменной передаются опции предустановки эксперимента
			# print experiment_options
			
			continue


	elif command == "kill-experiment":
		experiment_ether = data[1]
		# Если эксперимент есть в списке активных, то инициировать терминацию суброцесса
		if experiment_ether in present_experiments.keys():
			present_experiments[experiment_ether].terminate()
			print "Experiment ether #" + experiment_ether + " killed. " + "Bye child PID #" + str(present_experiments[experiment_ether].pid)
			# Обновить состояние словаря текущих экспериментов
			present_experiments.pop(experiment_ether)
			# Вернуть метку успешного завершения эксперимента в django
			client_socket.sendto("done", client_addr)
			client_socket.close()
			continue
							
		else:
			# Иначе вывести в stdout диспетчера ошибку
			print "No experiments with ether number " + experiment_ether
			client_socket.close()
			continue

	elif command == "del-results":
		buff_file = data[1]
		# Получение имени файла данных и рисунка
		file1 = os.path.join(WHERE_TO_STORE_DAT, buff_file) + ".dat"
		file2 = os.path.join(WHERE_TO_STORE_IMG, buff_file) + ".dat.png"
		file3 = os.path.join(WHERE_TO_STORE_DAT, buff_file) + ".dat.cfg"

		# Если файлы ещё существуют, то инициировать удаление
		if os.path.exists(file1):
			os.remove(file1)
		if os.path.exists(file2):
			os.remove(file2)
		if os.path.exists(file3):
			os.remove(file3)

		# Вернуть метку успешного завершения удаления файлов эксперимента в django
		client_socket.sendto("done", client_addr)
		client_socket.close()
		print "Deleted " + str(buff_file) + ".dat . cfg and .png if any"
		# print "View results request id" + experiment_ether

	else:
		client_socket.close()
		continue
	
	

manager_socket.shutdown(socket.SHUT_RDWR)
manager_socket.close()
print "Labmanager off"
