# -*- coding: UTF8 -*-
import sys, os
import signal
import time

import socket

import platform

import math
import Gnuplot

DEVICE_IP = '172.16.71.26'
# DEVICE_IP = '127.0.0.1'
DEVICE_PORT = 25000



def termHandler(signal, frame):
	g('set term png')
	g('set title "Hypersonic scanning experiment"')
	g('set xlabel "angle"')
	g('set ylabel "distance"')
	g('set polar')
	g('set angles degrees')
	g('set grid polar 10')
	g('set grid ls 10')
	g('set xrange [-80:80]')
	g('set yrange [0:80]')
	g('set xtics axis')
	g('set ytics axis')
	g('unset key')
	g('set xtics scale 0')
	g('set xtics ("" 10, "" 20, "" 30, "" 40, "" 50, "" 60, "" 70, "" 80)')
	g('set ytics 0, 10, 80')
	

	g('set out "' + ImgFilename + '.png"')
	# databuff = Gnuplot.File(DatFilename, using='1:2',with_='line')
	databuff = Gnuplot.File(DatFilename, using='2:3',with_='line lw 6')
	g.plot(databuff)
	g.close

	fo.close()
	# print 'TERMINATOR!!!'
	sys.exit(0)


def main():
	# time.sleep(5)

	args = sys.argv[1:]

	global DatFilename
	global ImgFilename
	DatFilename = args[0]
	# ImgFilename = os.path.join(os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), os.pardir)), 'exp_img') + "/" + os.path.basename(DatFilename)
	ImgFilename = os.path.join(os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), os.pardir)), 'exp_img', os.path.basename(DatFilename))

	if platform.system() == "Windows":
		import string
		DatFilename = string.replace(DatFilename, "\\", "/")
		ImgFilename = string.replace(ImgFilename, "\\", "/")

	print "Data file " + str(DatFilename)
	print "IMG file " + str(ImgFilename)

	t = 0
	angle = 6
	direction = 5

	global fo
	fo = open(DatFilename, "a")

	socket_to_device = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	socket_to_device.connect((DEVICE_IP, DEVICE_PORT))

	while 1:
		
		if t%2 == 0:
			g = Gnuplot.Gnuplot(persist=0)
			g('set term png')
			g('set title "Hypersonic scanning experiment"')
			g('set xlabel "angle"')
			g('set ylabel "distance"')
			g('set polar')
			g('set angles degrees')
			g('set grid polar 10')
			g('set grid ls 10')
			g('set xrange [-80:80]')
			g('set yrange [0:80]')
			g('set xtics axis')
			g('set ytics axis')
			g('unset key')
			g('set xtics scale 0')
			g('set xtics ("" 10, "" 20, "" 30, "" 40, "" 50, "" 60, "" 70, "" 80)')
			g('set ytics 0, 10, 80')
			

			g('set out "' + ImgFilename + '.png"')
			# databuff = Gnuplot.File(DatFilename, using='1:2',with_='line')
			databuff = Gnuplot.File(DatFilename, using='2:3',with_='line lw 6')
			g.plot(databuff)
			g.close
	
		socket_to_device.send("set angle " + str(angle))

		device_reply = socket_to_device.recv(1024)
		distance = device_reply.split()[0]

		print device_reply
		# device_reply = device_reply.split('\n')
		# distance = device_reply[1].split()[1]

		angle = angle + direction * (-1)

		if angle > 175:
			direction = direction * (-1)
		elif angle < 6:
			direction = direction * (-1)

		fo.write(str(t) + " " + str(angle) + " " + str(distance) + "\n")
		fo.flush()
		time.sleep(0.5)
		t+=0.5


if __name__ == "__main__":

	signal.signal(signal.SIGTERM, termHandler)

	main()
