# -*- coding: UTF8 -*-
import sys, os
import signal
import time

import socket

import math
import Gnuplot

DEVICE_IP = '172.16.71.26'
# DEVICE_IP = '127.0.0.1'
DEVICE_PORT = 25000



def termHandler(signal, frame):
	# g = Gnuplot.Gnuplot(persist=0)
	# g.title('My Systems Plot')
	# g.xlabel('Date')
	# g.ylabel('Value')
	# g('set term png')
	# g('set out' + '"' + ImgFilename + '.png"')
	# databuff = Gnuplot.File(DatFilename, using='1:2',with_='line', title="test")
	# g.plot(databuff)
	# g.close
	# fo.close()
	sys.exit(0)


def main():
	args = sys.argv[1:]
	
	global DatFilename
	global ImgFilename
	cfgFilename = str(args[0]) + ".cfg"
	returnedVals = str(args[0])

	print "Config file " + str(cfgFilename)

	time.sleep(0.5)

	while 1:

		fo = open(cfgFilename)
		exp_arguments = []
		for line in fo:
			exp_arguments.append(line)

		fo.close()

		socket_to_device = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		socket_to_device.connect((DEVICE_IP, DEVICE_PORT))

		# print "Exp_Type2.py reply:"
		socket_to_device.send("set laser " + str(exp_arguments[0]))
		device_reply = socket_to_device.recv(1024)
		# print device_reply
		socket_to_device.close()

		socket_to_device = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		socket_to_device.connect((DEVICE_IP, DEVICE_PORT))
		socket_to_device.send("set angle " + str(exp_arguments[1]))
		device_reply = ''
		device_reply = socket_to_device.recv(1024)		
		# print device_reply	
		socket_to_device.close()

		# сохранение результатов в файл данных
		fo = open(returnedVals, 'w')
		fo.write(device_reply)
		fo.close()

		time.sleep(0.5)	


if __name__ == "__main__":

	signal.signal(signal.SIGTERM, termHandler)

	main()
