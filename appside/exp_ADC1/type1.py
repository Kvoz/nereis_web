# import socket
import sys, os
import platform
import signal

import time

import math

import Gnuplot




# server_IP = "127.0.0.1"
# server_port = 25000

# def encConnect(server_IP, server_port):
# 	enc_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# 	enc_socket.connect((server_IP, server_port))
# 	return enc_socket

# def encGetStatus(enc_socket):
# 	msg = "Boot up!"
	
# 	enc_socket.send(msg)
# 	rx_data = enc_socket.recv(1024)
# 	return rx_data
# 	#print rx_data


def termHandler(signal, frame):
	g = Gnuplot.Gnuplot(persist=0)
	g.title('My Systems Plot')
	g.xlabel('Date')
	g.ylabel('Value')
	g('set term png')
	g('set out' + '"' + ImgFilename + '.png"')
	databuff = Gnuplot.File(DatFilename, using='1:2',with_='line', title="test")
	g.plot(databuff)
	g.close
	fo.close()
	# print 'TERMINATOR!!!'
	sys.exit(0)


def main():

	args = sys.argv[1:]
	
	global DatFilename
	global ImgFilename
	DatFilename = args[0]
	ImgFilename = os.path.join(os.path.abspath(os.path.join(os.path.dirname(os.path.dirname(__file__)), os.pardir)), 'exp_img', os.path.basename(DatFilename))

	if platform.system() == "Windows":
		import string
		ImgFilename = string.replace(ImgFilename, "\\", "/")

	print ImgFilename
	
	t = 0

	global fo
	fo = open(DatFilename, "a")

	while 1:
		y = math.sin(t*3 * math.pi / 180.0)		
		fo.write(str(t) + " " + str(y) + "\n")
		fo.flush()
		time.sleep(0.2)
		t+=1

		if t%5 == 0:
			g = Gnuplot.Gnuplot(persist=0)
			g.title('My Systems Plot')
			g.xlabel('Date')
			g.ylabel('Value')
			g('set term png')
			g('set out "' + str(ImgFilename) + '.png"')
			databuff = Gnuplot.File(DatFilename, using='1:2',with_='line', title="test")
			g.plot(databuff)
			g.close



if __name__ == "__main__":

	signal.signal(signal.SIGTERM, termHandler)

	main()
