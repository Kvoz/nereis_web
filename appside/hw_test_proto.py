# -*- coding: utf-8 -*-

import socket


manager_IP = "127.0.0.1"
manager_port = 25000


manager_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
manager_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
manager_socket.bind((manager_IP, manager_port)) 
manager_socket.listen(1)


while 1:
	
	(client_socket, client_addr) = manager_socket.accept()
	data = client_socket.recv(1024)
	data = data.split()

	
	if data[0] == "check" and data[1] == "status":
		print "Client string: " + str(data)
		client_socket.sendto("ready", client_addr)
		# client_socket.close()

	elif data[0] == "type2":
		print "Client string: " + str(data)
		client_socket.sendto("switched to type2", client_addr)
		# client_socket.close()

	elif data[0] == "set":
		print "Client string: " + str(data)

		if data[1] == "angle":
			client_socket.sendto("set servo angle: " + data[2], client_addr)	
		else:
			client_socket.sendto("set laser to state: " + data[1], client_addr)
		# client_socket.close()
	
	print "HW Process make a loop!"

	# else:
	# 	client_socket.close()

manager_socket.close()